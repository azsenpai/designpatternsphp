<?php

namespace DesignPatterns\Creational\FactoryMethod\Tests;

use DesignPatterns\Creational\FactoryMethod\FactoryMethod;
use DesignPatterns\Creational\FactoryMethod\ItalianFactory;
use DesignPatterns\Creational\FactoryMethod\GermanFactory;

/**
 *
 */
class FactoryMethodTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCanCreateCheapVehicleInGerman()
    {
        $factory = new GermanFactory();
        $vehicle = $factory->create(FactoryMethod::CHEAP);

        $this->assertInstanceOf('DesignPatterns\Creational\FactoryMethod\Bicycle', $vehicle);
    }

    /**
     *
     */
    public function testCanCreateFastVehicleInGerman()
    {
        $factory = new GermanFactory();
        $vehicle = $factory->create(FactoryMethod::FAST);

        $this->assertInstanceOf('DesignPatterns\Creational\FactoryMethod\CarMercedes', $vehicle);
    }

    /**
     *
     */
    public function testCanCreateCheapVehicleInItaly()
    {
        $factory = new ItalianFactory();
        $vehicle = $factory->create(FactoryMethod::CHEAP);

        $this->assertInstanceOf('DesignPatterns\Creational\FactoryMethod\Bicycle', $vehicle);
    }

    /**
     *
     */
    public function testCanCreateFastVehicleInItaly()
    {
        $factory = new ItalianFactory();
        $vehicle = $factory->create(FactoryMethod::FAST);

        $this->assertInstanceOf('DesignPatterns\Creational\FactoryMethod\CarFerarri', $vehicle);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage spaceship is not a valid vehicle
     */
    public function testUnknowType()
    {
        (new ItalianFactory())->create('spaceship');
    }
}
