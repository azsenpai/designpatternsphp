<?php

namespace DesignPatterns\Creational\FactoryMethod;

/**
 *
 */
class CarMercedes implements VehicleInterface
{
    /**
     * @var string
     */
    private $color;

    /**
     *
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     *
     */
    public function addAMGTuning()
    {
    }
}
