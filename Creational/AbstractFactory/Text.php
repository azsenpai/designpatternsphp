<?php

namespace DesignPatterns\Creational\AbstractFactory;

/**
 *
 */
abstract class Text
{
    /**
     * @var string
     */
    private $text;

    /**
     *
     */
    public function __construct($text)
    {
        $this->setText($text);
    }

    /**
     *
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     *
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}
