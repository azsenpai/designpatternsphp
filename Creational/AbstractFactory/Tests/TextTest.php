<?php

namespace DesignPatterns\Creational\AbstractFactory\Tests;

use DesignPatterns\Creational\AbstractFactory\JsonText;
use DesignPatterns\Creational\AbstractFactory\HtmlText;

/**
 *
 */
class TextTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCanCreateHtmlText()
    {
        $text = '<span class="status">ok</span>';

        $htmlText = new HtmlText($text);

        $this->assertInstanceOf('DesignPatterns\Creational\AbstractFactory\Text', $htmlText);
        $this->assertEquals($text, $htmlText->getText());
    }

    /**
     *
     */
    public function testCanCreateJsonText()
    {
        $text = '{"result":"ok"}';

        $jsonText = new JsonText($text);

        $this->assertInstanceOf('DesignPatterns\Creational\AbstractFactory\Text', $jsonText);
        $this->assertEquals($text, $jsonText->getText());
    }
}
