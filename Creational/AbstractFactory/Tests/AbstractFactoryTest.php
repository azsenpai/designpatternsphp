<?php

namespace DesignPatterns\Creational\AbstractFactory\Tests;

use DesignPatterns\Creational\AbstractFactory\JsonFactory;
use DesignPatterns\Creational\AbstractFactory\HtmlFactory;

/**
 *
 */
class AbstractFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCanCreateHtmlText()
    {
        $content = '<span class="status">ok</span>';

        $factory = new HtmlFactory();
        $text = $factory->createText($content);

        $this->assertInstanceOf('DesignPatterns\Creational\AbstractFactory\HtmlText', $text);
        $this->assertEquals($content, $text->getText());
    }

    /**
     *
     */
    public function testCanCreateJsonText()
    {
        $content = '{"status": "ok"}';

        $factory = new JsonFactory();
        $text = $factory->createText($content);

        $this->assertInstanceOf('DesignPatterns\Creational\AbstractFactory\JsonText', $text);
        $this->assertEquals($content, $text->getText());
    }
}
