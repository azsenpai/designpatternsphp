<?php

namespace DesignPatterns\Creational\StaticFactory;

use DesignPatterns\Creational\StaticFactory\StaticFactory;

/**
 *
 */
class StaticFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCanCreateNumberFormatter()
    {
        $this->assertInstanceOf('DesignPatterns\Creational\StaticFactory\FormatNumber', StaticFactory::factory('number'));
    }

    /**
     *
     */
    public function testCanCreateStringFormatter()
    {
        $this->assertInstanceOf('DesignPatterns\Creational\StaticFactory\FormatNumber', StaticFactory::factory('number'));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testException()
    {
        StaticFactory::factory('unknown');
    }
}
