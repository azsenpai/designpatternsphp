<?php

namespace DesignPatterns\Creational\StaticFactory;

/**
 *
 */
final class StaticFactory
{
    /**
     *
     */
    public static function factory($type)
    {
        switch ($type) {
            case 'number':
                return new FormatNumber();

            case 'string':
                return new FormatString();

            default:
                throw new \InvalidArgumentException('Unknow format given');
        }
    }
}
