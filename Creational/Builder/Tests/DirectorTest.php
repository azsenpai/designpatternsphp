<?php

namespace DesignPatterns\Creational\Builder\Tests;

use DesignPatterns\Creational\Builder\Director;
use DesignPatterns\Creational\Builder\TruckBuilder;
use DesignPatterns\Creational\Builder\CarBuilder;

/**
 *
 */
class DirectorTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCanBuildTruck()
    {
        $truckBuilder = new TruckBuilder();
        $truck = (new Director())->build($truckBuilder);

        $this->assertInstanceOf('DesignPatterns\Creational\Builder\Parts\Truck', $truck);
    }

    /**
     *
     */
    public function testCanBuildCar()
    {
        $carBuilder = new CarBuilder();
        $car = (new Director())->build($carBuilder);

        $this->assertInstanceOf('DesignPatterns\Creational\Builder\Parts\Car', $car);
    }
}
