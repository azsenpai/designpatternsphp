<?php

namespace DesignPatterns\Structural\Adapter\Tests;

use DesignPatterns\Structural\Adapter\Book;
use DesignPatterns\Structural\Adapter\Kindle;
use DesignPatterns\Structural\Adapter\EBookAdapter;

/**
 *
 */
class AdapterTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCanTurnPageOnBook()
    {
        $book = new Book();

        $book->open();
        $book->turnPage();

        $this->assertEquals(2, $book->getPage());
    }

    /**
     *
     */
    public function testCanTurnPageOnKindleLikeInANormalBook()
    {
        $kindle = new Kindle();
        $book = new EBookAdapter($kindle);

        $book->open();
        $book->turnPage();

        $this->assertEquals(2, $book->getPage());
    }
}
