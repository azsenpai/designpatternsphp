<?php

namespace DesignPatterns\Structural\Adapter;

/**
 *
 */
class EBookAdapter implements BookInterface
{
    /**
     * @var EBookInterface
     */
    private $eBook;

    /**
     * @var EBookInterface $eBook
     */
    public function __construct(EBookInterface $eBook)
    {
        $this->eBook = $eBook;
    }

    /**
     *
     */
    public function open()
    {
        $this->eBook->unlock();
    }

    /**
     *
     */
    public function turnPage()
    {
        $this->eBook->pressNext();
    }

    /**
     *
     */
    public function getPage()
    {
        return $this->eBook->getPage()[0];
    }
}
