<?php

namespace DesignPatterns\Structural\Adapter;

/**
 *
 */
class Kindle implements EBookInterface
{
    /**
     * @var int
     */
    private $page = 1;

    /**
     * @var int
     */
    private $totalPage = 100;

    /**
     *
     */
    public function pressNext()
    {
        $this->page ++;
    }

    /**
     *
     */
    public function unlock()
    {
    }

    /**
     * @return array
     */
    public function getPage()
    {
        return [$this->page, $this->totalPage];
    }
}
